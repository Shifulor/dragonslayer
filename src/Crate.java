import java.util.*;

public class Crate {
    boolean empty = false; //check if crate is empty
    static boolean preset_content = false;
    static String content;

    String search() {
        //50% chance
        if (preset_content == false) {
        if (new Random().nextBoolean()) {
        return ReturnCrateContents();
        } else {
            return "empty";
        }}
        return content;




    }

    private static String ReturnCrateContents() {
        String[] array=new String[]{"potion","weapon","key"};
        int rnd = new Random().nextInt(array.length);
        if (array[rnd] == "key") {
            return "key";

        } else if (array[rnd] == "weapon") {
            String[] weaponarray=new String[]{"sword","dagger","katana"};
            int randomweapon = new Random().nextInt(weaponarray.length);
            return weaponarray[randomweapon];


        }
        return array[rnd];
    }
    void set_content(String input_content) {
        content = input_content;
        preset_content = true;

    }
  
}
