public class Skeleton extends Mob {
    static int SkeletonHP = 100;
    static int SkeletonDMG = 20;

    int attack(int playerHP) {
        playerHP = playerHP - SkeletonDMG;
        if (playerHP <= 0) DragonSlayer.you_die();
        DragonSlayer.prettyprint("The Skeleton attacked you, you have  " + playerHP + "❤️ left.");

        return playerHP;
    }
    void get_attacked(int playerDMG) {
        if (!is_alive()) DragonSlayer.prettyprint("You have already killed the Skeleton");
        else {
        SkeletonHP = SkeletonHP - playerDMG;
        DragonSlayer.prettyprint("The Skeleton recieved " + playerDMG + "⚔️ from you and now has " + SkeletonHP + "❤️ left.");
        if (!is_alive()) DragonSlayer.prettyprint("congrats, you killed the skeleton");
        DragonSlayer.sleep(1000);

    }}
    Boolean is_alive() {
        if (SkeletonHP <= 0) {
            return false;
        }
        return true;

}}
