import java.util.*;


class DragonSlayer {
    //define arrays 
    //The check will always assume full UPPERCASE to avoid any case related issues
    private static final Set<String> yes_answers = Set.of(
        "Y","YEE","YES","YEP", "YAY", "YUP", "YEAH", "AYE", "YAH", "YAS"
    );
    private static final Set<String> no_answers = Set.of(
        "N","NO","NEVER","NOPE", "NAY", "NO WAY"
    );
    private static final Set<String> go_to_answers = Set.of(
            "GO TO", "GO", "WALK", "WALK TO", "RUN", "RUN TO", "MOVE TO", "SNEAK TO", "TRAVEL", "TRAVEL TO", "CONFRONT", "GO THROUGH"
    ); 
    private static final Set<String> open_answers = Set.of(
        "OPEN", "UNCLOSE"
    );
    private static final Set<String> attack_answers = Set.of(
        "ATTACK", "KILL", "FIGHT", "ASSAULT", "MURDER", "ASSASINATE", "HIT", "HURT", "STRIKE", "AGRESS"
    );
    private static final Set<String> search_answers = Set.of(
        "SEARCH", "SCAVENGE", "FOSSICK", "FORAGE", "INVESTIGATE", "SNOOP", "DIG THROUGH", "CHECK"
    );
    private static final Set<String> sleep_answers = Set.of(
    "SLEEP", "SLEEP IN", "REST", "REST IN", "NAP", "NAP IN", "LIE DOWN", "LIE DOWN IN"
    );
    


    static int current_room;
    static String input;
    public static int HP;
    public static int DMG;
    public static int KEYCOUNT;
    public static int POTIONCOUNT;
    static Scanner UI = new Scanner(System.in);
    public static int maxHP;

   

    static void sleep(int millis) {
        try {
            Thread.sleep(millis); //wait a bit to give user time to process
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static void clear() {
        System.out.print("\u001b[2J");
        System.out.print(HP + "❤️            " + DMG + "⚔️           " + KEYCOUNT +  "🗝️           " + POTIONCOUNT + "🧪" + "\n\n");
    }

    static void you_die() {
        clear();
        prettyprint("You have died, fare well traveler");
        System.exit(0);
    }

    static boolean match_expected_user_input(String expected_action, String expected_object, String user_input) {
        String[] inputarr = user_input.split(" ");
        String input_object = inputarr[inputarr.length-1];
        if (!input_object.toUpperCase().equals(expected_object.toUpperCase())) {

            return false;
        }

        //remove last item from that array
        String[] input_action_arr = Arrays.copyOf(inputarr, inputarr.length - 1);
        inputarr = null; //remove this variable early, not really used just checking Javas capabilities
        String input_action = String.join(" ", input_action_arr);

        if (expected_action == "go_to_answers") {
            if (go_to_answers.contains(input_action.toUpperCase())) {
                return true;
            } else return false;
        }  else if (expected_action == "open_answers") {
            if (open_answers.contains(input_action.toUpperCase())) {
                return true;
            } else return false;
        } else if (expected_action == "attack_answers") {
            if (attack_answers.contains(input_action.toUpperCase())) {
                return true;
            } else return false;
        } else if (expected_action == "search_answers") {
            if (search_answers.contains(input_action.toUpperCase())) {
                return true;
            } else return false;
        } else if (expected_action == "sleep_answers") {
            if (sleep_answers.contains(input_action.toUpperCase())) {
                return true;
            } else return false;
        } else {
            if (expected_action.equals(input_action)) return true;
            return false;
        }


    }
    /*
     * ASCII DRAGON ART BACKUP PROVIDED BY ASCIIART.EU
     * 
     *    (  )   /\   _                 (     
    \ |  (  \ ( \.(               )                      _____
  \  \ \  `  `   ) \             (  ___                 / _   \
 (_`    \+   . x  ( .\            \/   \____-----------/ (o)   \_
- .-               \+  ;          (  O                           \____
                          )        \_____________  `              \  /
(__                +- .( -'.- <. - _  VVVVVVV VV V\                 \/
(_____            ._._: <_ - <- _  (--  _AAAAAAA__A_/                  |
  .    /./.+-  . .- /  +--  - .     \______________//_              \_______
  (__ ' /x  / x _/ (                                  \___'          \     /
 , x / ( '  . / .  /                                      |           \   /
    /  /  _/ /    +                                      /              \/
   '  (__/                                             /                  \
     * 
     * 
     * 
     * 
     * 









     */
    static void print_logo() {
        System.out.println(" ____                              ____  _                                                        /\\   _                 (  ");   
        System.out.println("|  _ \\ _ __ __ _  __ _  ___  _ __ / ___|| | __ _ _   _  ___ _ __           _              \\ |  (  \\ ( \\.(               )                      _____");
        System.out.println("| | | | '__/ _` |/ _` |/ _ \\| '_ \\\\___ \\| |/ _` | | | |/ _ \\ '__|         / \\                \\  \\ \\  `  `   ) \\         (  ___                 / _   \\");
        System.out.println("| |_| | | | (_| | (_| | (_) | | | |___) | | (_| | |_| |  __/ |           |   |          (_`    \\+   . x  ( .\\            \\/   \\____-----------/ (o)   \\.");
        System.out.println("|____/|_|  \\__,_|\\__, |\\___/|_| |_|____/|_|\\__,_|\\__, |\\___|_|           |   |          - .-               \\+  ;        (  O                           \\____");
        System.out.println("                 |___/                           |___/                   |   |                                 )        \\_____________  `              \\  /");
        System.out.println("                                                                         |   |        (__                +- .( -'.- <. - _  VVVVVVV VV V\\                 \\/");
        System.out.println("                                                                          \\ /         (_____            ._._: <_ - <- _  (--  _AAAAAAA__A_/                  |");
        System.out.println("                                                                          ,|            .    /./.+-  . .- /  +--  - .     \\______________//_              \\_______");
        System.out.println("                                                                        0/               (__ ' /x  / x _/ (                                  \\___'          \\     /");
        System.out.println("                                                                       /7,              , x / ( '  . / .  /                                      |           \\   /");
        System.out.println("                                                                     .'(                   /  /  _/ /    +                                      /              \\/");
        System.out.println("                                                                   '^ / >                '  (__/                                             /                  \\\n\n");
    }
    public static void main(String args[]){
        System.out.print("\u001b[2J");
        print_logo();
        boolean UserInput = false; //little bool to repeat question until user answers the right thing
        while (!UserInput){ 
        prettyprint("Welcome to DragonSlayer, traveler. Are you brave enough to start your adventure?");
        System.out.print("$");
        input = UI.nextLine();
        if (yes_answers.contains(input.toUpperCase())) {
            prettyprint("Welcome aboard");
            sleep(200);
            UserInput = true;
        } else if (no_answers.contains(input.toUpperCase())) {
            prettyprint("Chickening out already, aye?");
            UserInput = true;

        }

    }

    //this is the first room. It will be a tutorial without much user choice
    // tutorial_room();
    // second_room();
     third_room();

    //exit program after this dialogue
    System.exit(0);



}

  static void prettyprint(String input_string) {
    for(int i = 0; i<input_string.length(); i++) {
        char to_print = input_string.charAt(i);
        System.out.print(to_print);
        sleep(90);

    }
    //add a \n at end
    System.out.println(System.lineSeparator());

}
static void tutorial_room() { //this function is always the same and never changes. All items will be processed manually and no external objects will be used
    HP = 10; 
    POTIONCOUNT++;
   clear();
   prettyprint("ROOM 1: tutorial");
   prettyprint("You wake up and slowly look around. Your head is hurting and everything looks hazy.");
   sleep(700);

   prettyprint("You see a crate, a door and a silhouette in a dark corner.");
   boolean UserInput = false;
   sleep(700);
   while (!UserInput){
    prettyprint("Warning: low health 10❤️ , you should use your potion");
    System.out.print("$");
    input = UI.nextLine();
    if (input.equals("use potion")) {
        prettyprint("Potion used, health restored to 100❤️");
        POTIONCOUNT--;
        sleep(1000);
        HP = 100;
        UserInput = true;
    } else {
        System.out.print("\u001b[2J");

    }
}
clear();
 UserInput = false;
   while (!UserInput){
    prettyprint("You do not have a weapon, you should search the crate for something useful");
    System.out.print("$");
    input = UI.nextLine();
    if (match_expected_user_input("search_answers", "crate", input)) {
        prettyprint("Dagger found: new weapon 15⚔️");
        sleep(1000);

        DMG = 15;
        UserInput = true;
    } else {
clear();
    }
}
//I hate this boilerplate code
//not anymore :D 
clear();
 UserInput = false;
   while (!UserInput){
    prettyprint("Let's try to escape the room through the door now.");
    System.out.print("$");
    input = UI.nextLine();
    if (match_expected_user_input("open_answers", "door", input)) {
        prettyprint("Cannot open door, door is locked");
        DMG = 15;
        UserInput = true;
        sleep(700);

    } else if (match_expected_user_input("go_to_answers", "door", input)) {
        prettyprint("The door is not open!");
        sleep(800);
        clear();
    }   else {
    clear();
    }
}
clear();
prettyprint("Oh no, it seems that the door is locked and we do not have a key");
 UserInput = false;
   while (!UserInput){
    prettyprint("Our only option seems to be to confront that mysterious silhouette");
    System.out.print("$");
    input = UI.nextLine();
    if (match_expected_user_input("go_to_answers", "silhouette", input)) {
        UserInput = true;
    } else {
    clear();
    }
}
prettyprint("You walk towards the dark corner to take a better look at the mysterious creature");
sleep(700);

prettyprint("It seems to be a small animal, almost like a ...");
System.out.println("A rat suddenly jumps and bites you with it's sharp teeth");
sleep(1000);
 UserInput = false;
   while (!UserInput){
    prettyprint("The rat attacks you again, you loose 5❤️ , you should defend yourself");
    HP = HP - 5;
    if (HP <= 0) {
        you_die();

    }
    System.out.print("$");
    input = UI.nextLine();
    if (match_expected_user_input("attack_answers", "rat", input)) {
        prettyprint("You attack the rat with your dagger, it instantly dies and drops a key. Well done traveler");
        KEYCOUNT++;
        sleep(200);

        UserInput = true;
    } else {
clear();
    }
}
UserInput = false;
while (!UserInput){
 prettyprint("Now that you have a key you can unlock the door");
 System.out.print("$");
 input = UI.nextLine();
 if (match_expected_user_input("unlock", "door", input)) {
     prettyprint("Used Key to unlock door");
     KEYCOUNT--;
     UserInput = true;
     sleep(700);

 } else {
clear();
 }
}
prettyprint("now that the door is unlocked your path to the next room is open.");
prettyprint("Tread carefully: from now on I can no longer guide you.");
prettyprint("GOOD LUCK!");
System.out.println("ROOM 1: tutorial ✅");
sleep(10000);



}
static void second_room() {
    clear();
    prettyprint("ROOM 2");
    prettyprint("After going through the door you see yourself in a new, bigger room");
    prettyprint("The door behind you magically closes again and you cannot return. ");
    prettyprint("You look around and see a door, a crate and a bed");
    prettyprint("But there is also something big walking towards you: A big skeleton is closing in on your location and doesn't seem to be glad you are here");
    Crate contains_better_weapon = new Crate();
    contains_better_weapon.set_content("sword");
    Skeleton enemy = new Skeleton();
    Door escapeDoor = new Door();

    while (true) {
    System.out.print("$");
    input = UI.nextLine();
    if (match_expected_user_input("attack_answers", "skeleton", input)) {
        if (enemy.is_alive()) {
        enemy.get_attacked(DMG);
        if (!enemy.is_alive()) {
            prettyprint("You have recieved 2 Keys");
            KEYCOUNT = KEYCOUNT + 2;
        }

        } else prettyprint("The skeleton is already dead");
    } else if (match_expected_user_input("search_answers", "crate", input)) {
    String earned_weapon = contains_better_weapon.search();
    process_weapon(earned_weapon);
    } else if (match_expected_user_input("sleep_answers", "bed", input)) {
        HP = Bed.sleep(HP);
    } else if (match_expected_user_input("unlock", "door", input)) {
        if (KEYCOUNT >= 1) {
            KEYCOUNT--;
            escapeDoor.unlock();
            sleep(1000);
            clear();
        } else {
            prettyprint("You have no key to unlock the door");
        }

    } else if (match_expected_user_input("open_answers", "door", input)) {
        if (escapeDoor.is_locked()) {
            System.out.print("The door is " + escapeDoor.is_locked());
            prettyprint("The door is locked") ;
    } else {
            escapeDoor.open();
            prettyprint("Opened the door");
        }

    } 
    else if (match_expected_user_input("go_to_answers", "door", input)) {
        if (escapeDoor.is_open()) {
        System.out.println("ROOM 1: tutorial ✅");
        System.out.println("ROOM 2: fight smart ✅");
        return;
        }
        else {
            prettyprint("The door is not open");
        }

        
    }
    else {
       clear();
    }
    if (enemy.is_alive()) HP = enemy.attack(HP);

    }


}

static void third_room() {
    //pre define some variables
    Door red_door = new Door(); //escape door
    Door blue_door = new Door();
    Door yellow_door = new Door(); //zombie
    Zombie zombie = new Zombie(); //behind yellow door
    //Stack<Mob> active_mobs = new Stack<Mob>();
    System.out.println("Room 3");
    prettyprint("You enter the third room and look around.");
    prettyprint("It looks like a little hall and is quite empty compared to the other rooms.");
    prettyprint("You see three doors in the hall, a yellow door, a blue door and a red door (interact with them by calling the Object \"Door\")s");

    while (true) {

    //this sections lets all active mobs make their move first
    if (yellow_door.is_open() && zombie.is_alive()) HP = zombie.attack(HP);


    System.out.print("$");
        input = UI.nextLine();
        if (match_expected_user_input("unlock", "door", input)) {       //if for unlocking door
            if (KEYCOUNT > 0) {
            //this is a bit messsy but should work
            Boolean user_anwered = false;
            while (user_anwered) {
            System.out.print("Please enter the color of the door you wish to unlock");
            System.out.print("$");
            input = UI.nextLine();
            if (input.toUpperCase().equals("YELLOW")) {
                yellow_door.unlock();
                KEYCOUNT--;

                user_anwered = true;
            } else if (input.toUpperCase().equals("RED")) {
                red_door.unlock();
                KEYCOUNT--;

                user_anwered = true;

            } else if (input.toUpperCase().equals("Blue")) {
                blue_door.unlock();
                KEYCOUNT--;
                user_anwered = true;

            }
        
        }
    } else {
        prettyprint("You have no key to unlock a door");
    }

    } else if (match_expected_user_input("open_answers", "door", input)) {  //if for opening door
            //this is a bit messsy but should work
            Boolean user_anwered = false;
            while (!user_anwered) {
            System.out.print("Please enter the color of the door you wish to unlock");
            System.out.print("$");
            input = UI.nextLine();
            if (input.toUpperCase().equals("YELLOW")) {
                if (!yellow_door.is_locked()) {
                if (!yellow_door.is_open()) {
                    yellow_door.open();
                    prettyprint("Opened yellow door");
                    prettyprint("A zombie walks out from behind the door and is coming closer");

                }
                } else prettyprint("The yellow door is locked");
                user_anwered = true;
            } else if (input.toUpperCase().equals("RED")) {
                if (!red_door.is_locked()) {
                    if (!red_door.is_open()) {
                        red_door.open();
                        prettyprint("Opened red door");
                        prettyprint("You see the next room and can now leave anytime");
                    }
                    } else prettyprint("The red door is locked");
                    user_anwered = true;
            } else if (input.toUpperCase().equals("BLUE")) {
                if (!blue_door.is_locked()) {
                    if (!blue_door.is_open()) {
                        blue_door.open();
                        prettyprint("Opened blue door");
                    }
                    } else prettyprint("The blue door is locked");
                    user_anwered = true;

            }
        
        }
    } else if (match_expected_user_input("attack_answers", "zombie", input) && yellow_door.is_open()) { //if for attacking zombie
    
        if (zombie.is_alive()) {

        zombie.get_attacked(DMG);
        if (!zombie.is_alive()) {
            prettyprint("recieved a key");
            KEYCOUNT++;
        }           
        }
         else {
            prettyprint("The Zombie is already dead");
        }
    }   else if (match_expected_user_input("go_to_answers", "door", input)) {  //if for going to door
        //this is a bit messsy but should work
        Boolean user_anwered = false;
        while (!user_anwered) {
        System.out.print("Please enter the color of the door you wish to walk to");
        System.out.print("$");
        input = UI.nextLine();
        if (input.toUpperCase().equals("YELLOW")) {
            if (!yellow_door.is_locked()) {
            if (yellow_door.is_open()) {
                prettyprint("The yellow door only contains a very small empty room");

            } else prettyprint("The yellow door is not open");
            } else prettyprint("The yellow door is locked");
            user_anwered = true;
        } else if (input.toUpperCase().equals("RED")) {
            if (!red_door.is_locked()) {
                if (red_door.is_open()) {
                    //end level
                    System.out.println("ROOM 1: tutorial        [✅]");
                    System.out.println("ROOM 2: fight smart     [✅]");
                    System.out.println("ROOM 3: making choices  [✅]");

                } else prettyprint("The red door is not open");
                } else prettyprint("The red door is locked");
                user_anwered = true;
        } else if (input.toUpperCase().equals("BLUE")) {
            if (!blue_door.is_locked()) {
                if (blue_door.is_open()) {
                    prettyprint("The blue door only contains a very small empty room");
                } else prettyprint("The blue door is not open");
                } else prettyprint("The blue door is locked");
                user_anwered = true;

        }
    
    }
    }

     else clear();
    
    
}

}


static void process_weapon(String weapon) {
    if (weapon == "dagger") DMG = DMG + 15;
    else if (weapon == "sword") DMG = DMG + 25;
    else if (weapon == "katana") DMG = DMG + 50;
    prettyprint("You have recieved a " + weapon + " and your damage is now " + DMG + "⚔️");


}
}