import java.util.*;

public class Zombie extends Mob {
    static int ZombieHP = 50;
    static int ZombieDMG = 100;
    static int distance = 0;

    int attack(int playerHP) {
        if (distance > 2) {
        playerHP = playerHP - ZombieDMG;
        DragonSlayer.prettyprint("The Zombie has bitten you, you have  " + playerHP + "❤️ left.");
        if (playerHP <= 0) DragonSlayer.you_die();
        DragonSlayer.prettyprint("The Virus of the Zombie is spreading in you, luck will decide your fate");
        DragonSlayer.sleep(1000);
        boolean rnd = new Random().nextBoolean();
        if (rnd) DragonSlayer.prettyprint("You survived");
        else DragonSlayer.you_die();

        return playerHP;
    } else {
        DragonSlayer.prettyprint("The Zombie is coming closer");
        distance++;
        return playerHP;
    }
}
    void get_attacked(int playerDMG) {
        if (!is_alive()) DragonSlayer.prettyprint("You have already killed the Zombie");
        else {
        ZombieHP = ZombieHP - playerDMG;
        DragonSlayer.prettyprint("The Zombie recieved " + playerDMG + "⚔️ from you and now has " + ZombieHP + "❤️ left.");
        if (!is_alive()) DragonSlayer.prettyprint("congrats, you killed the Zombie");
        DragonSlayer.sleep(1000);

    }}
    Boolean is_alive() {
        if (ZombieHP <= 0) {
            return false;
        }
        return true;

}}